<?php
namespace toolmodules\queue\command;

use toolmodules\queue\Worker;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Work extends AbstractCommand
{
    protected static $defaultName = 'queue:work';

    protected function configure()
    {
        $this->setDescription('Process the next job on a queue')
            ->setHelp('Process the next job on a queue')
            ->addArgument('connection', InputArgument::OPTIONAL, 'The name of the queue connection to work', null)
            ->addOption('queue', null, InputOption::VALUE_OPTIONAL, 'The queue to listen on')
            ->addOption('once', null, InputOption::VALUE_NONE, 'Only process the next job on the queue')
            ->addOption('delay', null, InputOption::VALUE_OPTIONAL, 'Amount of time to delay failed jobs', 0)
            ->addOption('force', null, InputOption::VALUE_NONE, 'Force the worker to run even in maintenance mode')
            ->addOption('memory', null, InputOption::VALUE_OPTIONAL, 'The memory limit in megabytes', 128)
            ->addOption('timeout', null, InputOption::VALUE_OPTIONAL, 'The number of seconds a child process can run', 60)
            ->addOption('sleep', null, InputOption::VALUE_OPTIONAL, 'Number of seconds to sleep when no job is available', 1)
            ->addOption('tries', null, InputOption::VALUE_OPTIONAL, 'Number of times to attempt a job before logging it failed', 0);
    }

    /**
     * Execute the console command.
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $input->getArgument('connection') ?: 'redis';

        $queue = $input->getOption('queue') ?: 'default';
        $delay = $input->getOption('delay');
        $sleep = $input->getOption('sleep');
        $tries = $input->getOption('tries');

        $output->writeln('Worker start ' . date('Y-m-d H:i:s'));
        $worker = new Worker();

        if ($input->getOption('once')) {
            $worker->runNextJob($connection, $queue, $delay, $sleep, $tries);
        } else {
            $memory  = $input->getOption('memory');
            $timeout = $input->getOption('timeout');
            $worker->daemon($connection, $queue, $delay, $sleep, $tries, $memory, $timeout);
        }

        return 0;
    }
}
