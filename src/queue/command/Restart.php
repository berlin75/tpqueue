<?php
namespace toolmodules\queue\command;

use toolmodules\Queue;
use toolmodules\queue\InteractsWithTime;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Restart extends AbstractCommand
{
    use InteractsWithTime;

    protected function configure()
    {
        $this->setName('queue:restart')
            ->setDescription('Restart queue worker daemons after their current job');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        Queue::setRestartTime($this->currentTime());
        $output->writeln("Broadcasting queue restart signal.");
        return 0;
    }
}
