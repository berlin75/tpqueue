<?php

namespace toolmodules\queue\command;

use toolmodules\supports\Arr;
use Symfony\Component\Console\Helper\Table;
use toolmodules\queue\command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListFailed extends AbstractCommand
{
    /**
     * The table headers for the command.
     *
     * @var array
     */
    protected $headers = ['ID', 'Connection', 'Queue', 'Class', 'Fail Time'];

    protected function configure()
    {
        $this->setName('queue:failed')
            ->setDescription('List all of the failed queue jobs');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (count($jobs = $this->getFailedJobs()) === 0) {
            $output->info('No failed jobs!');
            return;
        }
        $this->displayFailedJobs($jobs, $output);
        return 0;
    }

    /**
     * Display the failed jobs in the console.
     *
     * @param array $jobs
     * @return void
     */
    protected function displayFailedJobs(array $jobs, $output)
    {
        $table = new Table($output);
        $table->setHeaders($this->headers);
        $table->setRows($jobs);
        $table->render();
    }

    /**
     * Compile the failed jobs into a displayable format.
     *
     * @return array
     */
    protected function getFailedJobs()
    {
        return [
            [1, '2222', '33333', '4444', '555555']
        ];
        $failed = $this->app['queue.failer']->all();

        return collect($failed)->map(function ($failed) {
            return $this->parseFailedJob((array) $failed);
        })->filter()->all();
    }

    /**
     * Parse the failed job row.
     *
     * @param array $failed
     * @return array
     */
    protected function parseFailedJob(array $failed)
    {
        $row = array_values(Arr::except($failed, ['payload', 'exception']));

        array_splice($row, 3, 0, $this->extractJobName($failed['payload']));

        return $row;
    }

    /**
     * Extract the failed job name from payload.
     *
     * @param string $payload
     * @return string|null
     */
    private function extractJobName($payload)
    {
        $payload = json_decode($payload, true);

        if ($payload && (!isset($payload['data']['command']))) {
            return $payload['job'] ?? null;
        } elseif ($payload && isset($payload['data']['command'])) {
            return $this->matchJobName($payload);
        }
    }

    /**
     * Match the job name from the payload.
     *
     * @param array $payload
     * @return string
     */
    protected function matchJobName($payload)
    {
        preg_match('/"([^"]+)"/', $payload['data']['command'], $matches);

        if (isset($matches[1])) {
            return $matches[1];
        }

        return $payload['job'] ?? null;
    }
}
