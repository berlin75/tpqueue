<?php

namespace toolmodules\queue\listeners;

use toolmodules\supports\Log;
use toolmodules\queue\event\JobFailed;
use toolmodules\queue\event\JobProcessed;
use toolmodules\queue\event\JobProcessing;
use toolmodules\queue\event\WorkerStopping;
use toolmodules\queue\event\JobExceptionOccurred;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class KernelLogSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            JobExceptionOccurred::class => ['handleJobExceptionOccurred', 256],
            JobFailed::class => ['handleJobFailed', 256],
            JobProcessed::class => ['handleJobProcessed', 256],
            JobProcessing::class => ['handleJobProcessing', 256],
            WorkerStopping::class => ['handleWorkerStopping', 256],
        ];
    }

    public function handleJobExceptionOccurred(JobExceptionOccurred $event){
        $job = $event->job;
        $msg = sprintf( "ExceptionOccurred [%s] [%s]", $job->getJobId(), $job->getName() );
        Log::error($msg);
    }

    public function handleJobFailed(JobFailed $event){
        $job = $event->job;
        $e = $event->exception;
        $msg = sprintf( "Failed [%s] [%s], exception: %s", $job->getJobId(), $job->getName(),  $e->getMessage() . $e->getFile() . $e->getLine());
        Log::error($msg);
    }

    public function handleJobProcessed(JobProcessed $event){
        $job = $event->job;
        $msg = sprintf( "Processed [%s] [%s]", $job->getJobId(), $job->getName() );
        Log::debug($msg);
    }

    public function handleJobProcessing(JobProcessing $event){
        $job = $event->job;
        $msg = sprintf( "Processing [%s] [%s]", $job->getJobId(), $job->getName() );
        Log::debug($msg);
    }

    public function handleWorkerStopping(WorkerStopping $event){
        $msg = sprintf( "WorkerStopping status:[%s]", $event->status);
        Log::error($msg);
    }
}
