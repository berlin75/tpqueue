<?php

namespace toolmodules\queue\event;

use toolmodules\queue\Job;

class JobFailed extends Event
{
    /** @var string */
    public $connection;

    /** @var Job */
    public $job;

    /** @var \Exception */
    public $exception;

    public function __construct($connection, $job, $exception)
    {
        $this->connection = $connection;
        $this->job        = $job;
        $this->exception  = $exception;
    }
}
