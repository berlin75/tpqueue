<?php

namespace toolmodules\queue\event;

use toolmodules\queue\Job;

class JobProcessing extends Event
{
    /** @var string */
    public $connection;

    /** @var Job */
    public $job;

    public function __construct($connection, $job)
    {
        $this->connection = $connection;
        $this->job        = $job;
    }
}
