<?php

namespace toolmodules\queue\event;

class WorkerStopping extends Event
{
    /**
     * The exit status.
     *
     * @var int
     */
    public $status;

    /**
     * Create a new event instance.
     *
     * @param int $status
     * @return void
     */
    public function __construct($status = 0)
    {
        $this->status = $status;
    }
}
