<?php
namespace toolmodules\queue;

class CallQueuedHandler
{
    protected $app;

    public function call(Job $job, array $data)
    {
        $command = unserialize($data['command']);
        $command->handle();

        if (!$job->isDeletedOrReleased()) {
            $job->delete();
        }
    }

    public function failed(array $data)
    {
        $command = unserialize($data['command']);

        if (method_exists($command, 'failed')) {
            $command->failed();
        }
    }
}
