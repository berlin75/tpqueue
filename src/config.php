<?php

return [
    'default' => 'redis',
    'connections' => [
        'redis' => [
            'type'       => 'redis',
            'queue'      => 'default',
            'host'       => '127.0.0.1',
            'port'       => 6379,
            'password'   => '',
            'select'     => 0,
            'timeout'    => 0,
            'persistent' => false,
        ],
    ],
    'failed' => [
        'type'  => 'none',
        'table' => 'failed_jobs',
    ],
    'logger' => [
        'file'      => __DIR__ . "/../runtime/queue/queue.log",
        'identify'  => 'queue',
        'level'     => \Monolog\Logger::DEBUG,
        'type'      => 'daily',
        'max_files' => 30,
    ],
];
