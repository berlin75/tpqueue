<?php
namespace toolmodules;

use toolmodules\Events;
use toolmodules\supports\Log;
use toolmodules\supports\Logger;
use toolmodules\queue\Connector;
use toolmodules\queue\connector\Redis;
use toolmodules\queue\connector\Database;
use toolmodules\queue\listeners\KernelLogSubscriber;

/**
 * Class Queue
 *
 * @mixin Database
 * @mixin Redis
 */
class Queue
{
    /** @var Connector */
    protected static $connector;

    public static function getConnector() {
        if (!self::$connector) {
            $options = [];
            $confiPath = __DIR__ . '/../../../../';
            if (file_exists($confiPath.'config/queue.php')) {
                $options = require $confiPath.'config/queue.php';
            } elseif (file_exists($confiPath.'conf/queue.php')) {
                $options = require $confiPath.'conf/queue.php';
            }

            $config = require 'config.php';
            $config = array_merge($config, $options);
            $type = $config['default'] ?? 'redis';
            
            $class = false !== strpos($type, '\\') ? $type : '\\toolmodules\\queue\\connector\\' . ucfirst($type);
            self::$connector = new $class($config['connections'][$type]);

            self::registerLogService($config['logger']);
            self::registerEventService();
        }
        return self::$connector;
    }

    public static function __callStatic($name, $arguments) {
        return call_user_func_array([self::getConnector(), $name], $arguments);
    }

    /**
     * Register log service.
     * @throws Exception
     */
    protected static function registerLogService($config) {
        $logger = new Logger();
        $logger->setConfig($config);
        Log::setInstance($logger);
    }

    /**
     * Register event service.
     */
    protected static function registerEventService() {
        Events::setDispatcher(Events::createDispatcher());
        Events::addSubscriber(new KernelLogSubscriber());
    }
}
