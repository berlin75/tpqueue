<?php
namespace toolmodules;

class Test {
    public static function test() {
        if(mt_rand(0, 10) > 5){
            Queue::push('\toolmodules\Test@consume', json_encode([date('Y-m-d H:i:s'), 'test push']));
            echo date('Y-m-d H:i:s') . ' queue push success';
        }else{
            Queue::later(60, '\toolmodules\Test@consume', json_encode([date('Y-m-d H:i:s'), 'test later']));
            echo date('Y-m-d H:i:s') . ' queue later success';
        }
    }

    public function consume($job, $params){
        //....这里执行具体的任务 
            
        if ($job->attempts() > 3) {
            //通过这个方法可以检查这个任务已经重试了几次了
            $job->delete();
        }
        
        
        //如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
        $job->delete();
        
        // 也可以重新发布这个任务
        //   $job->release($delay); //$delay为延迟时间
        
        echo date('Y-m-d H:i:s') . ' queue consume success' . json_encode($params) . PHP_EOL;
    }
}