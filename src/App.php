<?php
namespace toolmodules;

use Symfony\Component\Console\Application as BaseApplication;
use toolmodules\queue\command\Listen;
use toolmodules\queue\command\ListFailed;
use toolmodules\queue\command\Restart;
use toolmodules\queue\command\Work;

class App extends BaseApplication
{
    public function __construct()
    {
        parent::__construct('Queue Console Tool');

        $this->addCommands([
            new Work(),
            new Listen(),
            new Restart(),
            new ListFailed(),
        ]);
    }
}